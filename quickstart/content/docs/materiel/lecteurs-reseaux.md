# Tuto installation lecteurs réseaux
## Utilisation des lecteurs réseaux de l'école
Via des partages windows, l'école met à disposition des données accessibles via le réseau.

Ce tutoriel a pour but de les installer de façon définitive sur votre machine. 

## Sur windows
- Cliquer droit sur "ce Pc" puis "ajouter un emplacement réseau

{{< imgs src="/img/Ajouter.png" >}}

- Suivant, suivant. Adresse internet (refaire la manip pour ajouter tout les lecteurs dont vous avez besoin) :

        \\gcp-ens.emse.fr\soft enseignement pour le disque contenant les vm et les logiciels école
        \\gcp-ens.emse.fr\Photocopieur\ELEVES pour le dossier photocopieur
        \\gcp-ens.emse.fr\ismin pour le dossier partagé de la promo (utile pour les TD/TP)

- suivant, entrer le nom souhaité et cliquer sur terminer