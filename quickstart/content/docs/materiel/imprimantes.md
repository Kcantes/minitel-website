## Utilisation des imprimantes

{{< tabs "imprimantes" >}}

{{< tab "Installation sur windows" >}} 

-  Ouvrir le lecteur réseau 

        \\192.168.130.2
-  se connecter sur le lecteur réseau: mettre vos prenom.nom@etu.emse.fr
{{< imgs src="/img/Connexion-lecteur-reseau.png" >}}

-  Double-cliquer sur "imprimantes-gpc"

-  Voilà! tu viens d'installer le driver de l'imprimante sur ton pc
{{< imgs src="/img/Lecteur-reseau.png" >}}

{{< /tab >}}
{{< tab "Installation sur linux" >}}
- On ajoute l'imprimante réseau > Imprimante Windows via SAMBA 
    
    smb://192.168.130.2/imprimantes-gcp

- On spécifie le pilote voulu (dans le cas de notre test) 
https://www.openprinting.org/download/PPD/Ricoh/PS/Ricoh-MP_C3004_PS.ppd

- Faire attention à la première impression il faut s'authentifier sur la file d'impression de linux.

- ​clique droit sur votre imprimante et selectionnez voir la file d'attente puis , clique droit sure la file d'attente et selectionnez authentification 
{{< imgs src="/img/Linux.png" >}}


{{< /tab >}}
{{< tab "OSx" >}}

PAS ENCORE REDIGE

{{< /tab >}}
{{< /tabs >}}

## Récupérer ton code PUK
- avec un navigateur internet (de préférence firefox ou internet explorer), se connecter sur http://192.168.130.2/watchdoc
{{< imgs src="/img/Watchdoc.png" >}}

-      Noter le code puk
{{< imgs src="/img/Code-puk.png" >}}

- NB : Sur ce site tu trouveras la liste des documents que tu as imprimmé
- Lors de la première utilisation du copieur:

    - passer le badge sur le copieur puis suivre les instructions
    {{< imgs src="/img/Copieur1.png" >}}
    {{< imgs src="/img/Copieur2.png" >}}
    {{< imgs src="/img/Copieur3.png" >}}

- A Noter : Pour utiliser la fonction "scan to folder" merci de choir le répertoire réseau destinataire correspondant a votre entité (ELEVES), ensuite les documents seront a récupérer a cette adresse : 

        \\sgc.emse.fr\Photocopieur\... 
puis choisir votre entité (pour nous c'est ELEVES). Les documents ne doivent pas être stockés dans ce répertoire qui sera régulièrement nettoyé (vous avez les droits pour supprimer des fichiers dans ce dossier).

## Impression
Il ne reste plus qu'à utiliser l'imprimante. Pour cela, il suffit de sélectionner 'imprimantes-gcp sur imprimantes-gcp' lors d'une impression sur word ou acrobat reader. Il faut ensuite badger et selectionner le document à imprimer sur l'imprimante. 