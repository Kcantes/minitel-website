# Utilisation-seafile
Seafile est un logiciel de cloud vous permettant de sauvergarder et partager des données. Celles-ci seront stockées sur les serveurs de l'école. Seafile intègre également un gestionnaire de version, ce qui vous sera utile dans vos projets

## Installation du client
Télécharger le client à l'adresse suivante: https://www.seafile.com/en/download/
Lancer le client et ajouter un compte avec les paramètres suivants (identifiants école):

{{< imgs src="/img/seafile0.png" >}}

- Une fois connecté vous pouvez choisir ou:
    - de créer un dossier sur votre disque qui sera synchronisé avec votre seafile
    - de sélectionner un dossier existant sur votre disque dur. les données présentes seront ensuite envoyées sur le serveur
- NB: Si des dossiers sont partagés avec vous sur seafile, vous pouvez cliquer dessus pour les synchroniser. Puis vous aurez à nouveau le choix de créer un dossier (conseillé) ou synchroniser avec un dossier existant

{{< imgs src="/img/Synchro.png" >}}


## Utilisation de l'interface web

### Partager une bibliothèque
- Si vous souhaitez partager l'une de vos bibliothèque, il vous suffit d'aller sur l'interface web https://seafile.emse.fr/ et d'ouvrir la bibliothèque que vous souhaitez partager.
- Cliquer ensuite sur "partager", vous avez ensuite le choix entre différentes options:
    - Lien de téléchargement, qui permettra de donner l'accès au dossier à n'importe qui disposant du lien (un client PI par exemple)
    - Lien d'envoi, qui permettra à toute personne possédant le lien d'envoyer du contenu dans le dossier
    - Partager avec l'utilisateur, permettant de saisir l'utilisateur avec qui vous voulez partager le dossier. Attention Il faudra que cet utilisateur se soit déjà connecté une fois sur l'interface web de seafile (ce qui aura pour effet de créer un compte)
- **NB**: Lorsque tu cliques sur partager, tu partage le dossier dans lequel tu te trouve. Il est donc possible de ne partager qu'un seul sous-dossier de la bibliothèque.

### Création d'un groupe d'utilisateur
Cela peut vous être utile pour partager des données lorsque vous faites un projet avec d'autres personnes ou dans le cadre associatif.
- Aller sur https://seafile.emse.fr/#groups/
- Cliquer sur "nouveau groupe" et le nommer
- Cliquer sur le nom du groupe créé (ici "test")
{{< imgs src="/img/Groupes.png" >}}

- Cliquer sur "nouvelle bibliothèque". Celle-ci contiendra les fichiers partagés avec le groupe. Vous pouvez mettre un mot de passe à celle-ci
- Cliquer sur "paramètres" puis "gestion des membres" afin d'ajouter des membres à ce groupe.
{{< imgs src="/img/Membres.png" >}}

- Il suffira d'écrire le nom et prénom de la personne dans le champ de recherche et de l'ajouter. Attention il aura fallu que cette personne se connecte auparavent sur https://seafile-ens-gcp.emse.fr sinon ça ne fonctionnera pas

### Utilisation de l'historique
Comme dit plus haut il est possible de consulter l'historique d'un fichier, en cliquant sur historique.

{{< imgs src="/img/Historique.png" >}}

Pour restaurer une version, rien de plus simple, il suffit de cliquer sur restaurer.

{{< imgs src="/img/Restaurer.png" >}}
