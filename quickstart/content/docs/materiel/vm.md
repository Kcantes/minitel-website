# Tuto machine virtuelle
##  Installation d'une machine virtuelle 
{{< hint info >}}
En informatique, une machine virtuelle (VM) est une illusion d'un appareil informatique créée par un logiciel d'émulation. Le logiciel d'émulation simule la présence de ressources matérielles et logicielles telles que la mémoire, le processeur, le disque dur, voire le système d'exploitation et les pilotes, permettant d'exécuter des programmes dans les mêmes conditions que celles de la machine simulée. 

Un des intérêts des machines virtuelles est de pouvoir s'abstraire des caractéristiques de la machine physique utilisée (matérielles et logicielles — notamment système d'exploitation), permettant une forte portabilité des logiciels et la gestion de systèmes hérités étant parfois conçus pour des machines ou des environnements logiciels anciens et plus disponibles. 

{{< /hint >}}

source : [wikipédia](https://fr.wikipedia.org/wiki/Machine_virtuelle) 

## Matériel nécessaire 
-     Le logiciel virtualBox préalablement installé (si tu ne l'as pas fait tu peux le faire ici)
-    Le fichier .ova de ta machine virtuelle préalablement téléchargé

## Installation
1. lancez le logiciel virtualBox et cliquez sur fichier>importer un appareil virtuel
{{< imgs src="/img/Fichier.png" >}}

2. lancez le logiciel virtualBox et cliquez sur fichier>importer un appareil virtuel
{{< imgs src="/img/Importer1.png" >}}
{{< imgs src="/img/Importer1bis.png" >}}

3. cliquez sur importer
{{< imgs src="/img/Importer2.png" >}}

4. Venez nous voir sur notre page facebook le temps du chargement
{{< imgs src="/img/Chargement.png" >}}

5. Une fois le fichier chargé cliquez sur configuration
{{< imgs src="/img/Chargement.png" >}}

    - Sur l'onglet réseau, passez en NAT
    {{< imgs src="/img/Reseau.png" >}}

    - Sur l'onglet USB décochez la case "activer le contrôleur USB"
    {{< imgs src="/img/Usb.png" >}}

6. Il ne vous reste plus qu'à lancer votre machine virtuelle et enlever les premiers warning
{{< imgs src="/img/Demarrer.png" >}}
{{< imgs src="/img/Warning.png" >}}


## Et voilà votre machine est prête à utilisation 
{{< imgs src="/img/Ubuntu_final.png" >}}
