# Eduroam
## Utilisation du réseau eduroam
Le réseau wifi eduroam est disponible à l'école et à la maison des élèves. Il permet d'accéder à internet. Afin de pouvoir s'y connecter il faut suivre une démarche spécifique à votre périphérique

{{< tabs "eduroam" >}}

{{< tab "PC" >}}
choisir le réseau eduroam
{{< imgs src="/img/Capturepc.png" >}}
rentrer votre adresse école et votre mot de passe école


{{< /tab >}}

{{< tab "Téléphone" >}}
Selectionner dans les réseaux wifi disponibles le réseau eduroam. Régler les paramètres comme sur l'image ci-dessous
{{< imgs src="/img/eduroamtel.png" >}}
entrer votre adresse mail école et mot de passe école, et connectez-vous



{{< /tab >}}


{{< /tabs >}}

## Particularité de la residence

Une fois connecté sur le réseau eduroam de la résidence, il faudra quand même vous connecter sur le portail captif. (https://fw-cgcp.emse.fr/auth/)