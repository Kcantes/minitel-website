# Creation chroot
## Introduction
Vous souhaitez créer un accès sftp sur votre machine mais vous ne souhaitez pas que cet utilisateur puisse avoir accès à tous les fichier ? Le chroot est fait pour vous.

## Installation
### Prérequis
Installer un serveur OpenSSH sur sa machine : 
    
    root@minitel>>> apt-get update;apt-get upgrade;apt-get install openssh-server

### Configuration
**Ajout d'un utilisateur**

- Sous linux, l'ajout d'un utilisateur se fait à l'aide de la commande : 

        root@minitel>>> adduser nom_utilisateur
Il vous sera ensuite demandé de rentrer le mot de passe associé à ce compte.

- Si il n'est pas présent, il faut créer le répertoire /home/nom_utilisateur/ grâce à la 
        
        commande root@minitel>>> mkdir /home/nom_utilisateur

**Configuration d'OpenSSH**
Dans le fichier 

    nano /etc/ssh/sshd_config

rajouter/dé-commenter/modifier les lignes suivantes

    port 22
    PermitRootLogin no
    AllowUsers nom_utilisateur root
    [...]
    UsePAM yes
    [...]
    Match user nom_utilisateur
        ChrootDirectory /home/nom_utilisateur/
        ForceCommand internal-sftp
        AllowTCPForwarding no

**Explications**

- port 22 : définit la valeur d'écoute du service ssh.
- PermitRootLogin no  : La valeur 'no' indique qu'il ne sera pas possible de se connecter en tant que root sur le serveur. Il faudra se connecter en tatn qu'utilisateur qui lui deviendra root (utlisateur@minitel>>>su - ).


**Attention** ! Soyez sûr qu'un autre utilisateur est configuré sur le serveur. En effet, par la suite, l'utilisateur nom_utilisateur n'aura plus accès au ssh. Si ce n'est pas le cas, alors changer le no par yes (déconseillé).

- AllowUsers : définit les utilisateurs ayant le droit de se connecter au serveur par ssh. Dans notre cas il y a uniquement nom_utilisateur et root.
- Match user : indique que les instructions suivantes seront destiné aux utilisateurs passés en paramètres. Ici les instructions seront destinés à l'utilisateur nom_utilisateur
- ChrootDirectory : cloisonne la connexion de l'utilisateur visé dans le dossier indiqué. Ici /home/nom_utilisateur/.
- ForceCommand internal-sftp
- AllowTCPForwarding
- On redémarre le service avec la commande service ssh restart

**Mise en place des droits**

- Dans un premier temps, nous allons modifier les droits du répertoire /home/nom_utilisateur/. Pour cela on tape la commande suivante

        root@minitel>>> chown root:root /home/nom_utilisateur/

A ce stade, nom_utilisateur a accès au répertoire /home/nom_utilisateur/ mais ne peut rien y faire. Pour cela nous allons lui créer un répertoire ou elle pourra y faire ce qu'elle veut (avec des droits restreints quand même).

- On créer ce dossier grâce à la commande root@minitel>>> mkdir /home/nom_utilisateur/jail/
- On attribut les droits à nom_utilisateur : root@minitel>>> chown nom_utilisateur:nom_utilisateur /home/nom_utilisateur/jail/. On en profite pour exécuter cette commande root@minitel>>> chmod -R 755 /home/nom_utilisateur/jail/
- Comme promis, on coupe l'accès au shell à nom_utilisateur : root@minitel>>> chsh nom_utilisateur -s /bin/true

## Source
[debian-facile.org](http://debian-facile.org/doc:reseau:ssh:tp-sftp-via-openssh-server)