# Creation VPN
## Introduction
Un VPN ou Virtual Private Network est un moyen permettant de "relier" deux ordinateur distants. Le VPN permet entre autre de gérer des parcs informatiques distant comme si ils étaient à coté. Pour vous, Etudiant des Mines De Saint-Etienne, il permet surtout de by-passer les restriction mise en place sur le réseau de la résidence. Le VPN permet de mettre deux ordinateur distant sur le même réseau local grâce à l'établissement d'un tunnel entre les deux machines.

## Prérequis
- Un ordinateur sous OS quelconque
- Un serveur sous Debian Jessie ou équivalent
- De la patience

## Installation ( Serveur )
### Un peu de ménage
- On commence par télécharger OpenVPN sur le serveur : root@minitel>>> apt-get update;apt-get upgrade; apt-get install openvpn
- On fait un peu de ménage pour rendre l'exercice plus facile. Pour cea on récupère les script easy-rsa qui permettront de générer les certificats. Ils sont en général place dans /usr/share/easy-rsa mais ce n'est pas une obligation. Sous certaines versions de Debian ils sont dans /usr/share/openvpn/doc/easy-rsa/2.0:

        root@minitel>>> cp -a /usr/share/easy-rsa /etc/openvpn/ root@minitel>>> cd /etc/openvpn/easy-rsa

- On initialise les variables des certificats et on purge les anciennes clés : root@minitel>>>source vars;./clean-all

### Nos premiers certificats
- On créer le certificat de l'autorité de certification : 
        
        ./build-ca

Vous devriez avoir dans le dossier /etc/openvpn/easy-rsa/keys les fichier ca.crt et ca.key

- Afin de fluidifier les échanges entre le serveur et le client tout en maximisant la sécurité, on génère la clé Diffie-Hellman :

        ./build-dh

- On génère ensuite les certificats du serveur : 
        
        ./build-key-server nom_serveur

### Notre configuration serveur
- On copie le modèle de configuration serveur serveur.conf d'OpenVpn dans le dossier /etc/openvpn/ :

        gunzip -c /usr/share/doc/openvpn/examples/sample-config-files/server.conf.gz > /etc/openvpn/server.conf

- On l'édite de tel façon que les lignes suivantes soit dé-commentés :
   
        user nobody
        group nogroup
        comp-lzo
        ca /etc/openvpn/easy-rsa/keys/ca.crt
        cert /etc/openvpn/easy-rsa/keys/nom_serveur.crt
        key /etc/openvpn/easy-rsa/keys/nom_serveur.key # This file should be kept secret
        dh /etc/openvpn/easy-rsa/keys/dh2048.pem
        push "redirect-gateway def1 bypass-dhcp"
        push "dhcp-option DNS 8.8.8.8"
        push "dhcp-option DNS 8.8.4.4"

Ici on a utilisé les DNS de google, rien ne vous empèche d'en utiliser d'autre.

- On teste la configration cd /etc/openvpn/; systemctl start openvpn@server

On préfèrera systemctl à service car il permet d'indiquer explicitement la configuration à utiliser. On peut donc ainsi lancer plusieurs VPN sur le même serveur.

- root@minitel>>> ifconfig tun0 Si rien ne sort de cette commande, alors recommencez le tuto, sinon vous pouvez passer à la suite.

### Un peu de réseau
- Afin de permettre la translations des paquets de l'interface tun0 à l'interface réseau primaire du serveur, nous allons devoir natter certains paquets.

        root@minitel>>> echo "net.ipv4.ip_forward=1" > /etc/sysctl.d/NAT.conf; sysctl -p /etc/sysctl.d/NAT.conf; iptables -t filter -P FORWARD ACCEPT; iptables -t filter -A INPUT -p udp --dport 1194 -j ACCEPT; iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE; Pour plus d'information sur ces commandes je vous invite à regarder ce site.

- On rend ces règles persistante : 

        root@minitel>>> iptables-save > /etc/iptables.rules

## Notre configuration client

- On se place dans le dossier easy-rsa de OpenVpn : 

        root@minitel>>> cd /etc/openvpn/easy-rsa/
- On génère les clé et certificat du client : 

        root@minitel>>> ./build-key nom_client
- On récupère un template de configuration client : 

        root@minitel>>> cp /usr/share/doc/openvpn/examples/sample-config-files/client.conf /etc/openvpn/
- On copie ensuite les fichiers ca.crt, nom_client.crt, nom_client.key sur le PC du client. On oublie pas de prendre le template de la configuration client
- Dans la configuration client qui est maintenant sur le PC du client, on change l'adresse du serveur par son ip, et on modifie les chemins d'accès aux fichiers ca.crt, nom_client.crt, nom_client.key. Si vous ne souhaitez pas avoir plusieurs fichiers il est tout à fait possible de tout mettre dans le fichier de configuration client. Dans ce cas la, il suffit de copier le contenue des fichiers ci-dessus nommé à la fin du fichier de configuration entre respectivement les balises <ca>contenu ca.crt</ca> <cert>contenu nom_client.crt</cert> <key>contenu nom_client.key</key>
- On ajoute ensuite cette ligne à la fin du fichier : redirect-gateway def1.


Si vous êtes sous Windows, renommez ce fichier en config.ovpn et placé le dans le dossier /config de OpenVpn

## Source
[debian-facile.org](https://debian-facile.org/doc:reseau:vpn:openvpn)