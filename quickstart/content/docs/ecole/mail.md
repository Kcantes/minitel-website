# Param Client Mail

Les mails école n'étant sans doutes pas vos seuls mails et le fait de devoir passer par une interface web pouvant être frustrant, l'idéal est d'utiliser un client mail installé sur ta machine comme Thunderbird (gratuit) ou Microsoft Outlook (gratuit pendant tes années à l'EMSE) ou l'application mail de ton téléphone.

Cette page ne donne que les paramètres serveurs à rentrer dans le client mail pour que ça fonctionne.

## Prérequis

- Savoir comment ajouter un compte mail sur son client mail
- Savoir comment passer en réglage manuel lors de l'ajout du compte mail
- Connaitre son mot de passe école (merci captain obvious)

## Paramètres
- Email : ton mail
- Mot de passe : ton mot de passe école
- Serveur entrant : IMAP ou POP3, selon préférence (IMAP recommandé)
        
        Nom d'hote : messel.emse.fr
        Port : 993
        Sécurité : ssl/tls
        Méthode d'authentification : mot de passe normal
        Identifiant : prenom.nom (/!\ sans le @etu.emse.fr)

- Serveur sortant :

        Nom d'hôte : messel.emse.fr
        Port : 465
        Sécurité : ssl/tls
        Méthode d'authentification : mot de passe normal
        Identifiant : prenom.nom (/!\ sans le @etu.emse.fr)