# Agenda-telephone
## Guide pour avoir son agenda école sur le téléphone
Ce guide est destiné à vous donner les grandes étapes pour avoir l'agenda de l'école sur son téléphone. Avec l'extension google agenda sur thunderbird il est également possible d'avoir l'agenda de l'école dans thunderbird

{{< imgs src="/img/Agenda-telephone.png" width="370" height="640">}}


## Prérequis 
-    posséder un compte google ou un cloud équivalent tel que owncloud
-    savoir synchroniser ses agendas google avec son téléphone- Serveur entrant : IMAP ou POP3, selon préférence (IMAP recommandé)


## Import de l'ical dans google agenda
**1.  Il faut tout d'abord récupérer l'ical sur promethee**

{{< imgs src="/img/Promethee.png" >}}

**2.  copier le lien en vert. la procédure d'import dans google agenda est également expliquée sur cette page**

{{< imgs src="/img/Lien.png" >}}

**3.  aller sur google agenda : https://calendar.google.com/calendar**

{{< imgs src="/img/Googleagenda.png" >}}

**4.  puis coller le lien**

Si vous avez activé la synchronisation d'agenda sur votre téléphone android, cela va fonctionner. 