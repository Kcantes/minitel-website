# Liste de logiciels recommandés

Voici une petite liste de logiciels que nous vous recommandons d'utiliser personnellement ou pour le travail. Si vous avez des suggestions, n'hésitez pas à nous les soumettre !

{{< tabs "uniqueid" >}}

{{< tab "Windows" >}} 

## Suites de bureautique
- Microsoft Office (license gratuite avec l'école !)
- Libreoffice (open-source <3) https://fr.libreoffice.org/
- MiKTeX (une des suites pour LaTeX, difficile à maitriser mais très puissant pour écrire un rapport) http://miktex.org/

## Compléments en bureautique
- Adobe Acrobat Reader DC (gratuit et efficace) https://get.adobe.com/fr/reader/
- Foxit Reader (gratuit et léger !) https://www.foxitsoftware.com/fr/products/pdf-reader/
- PDF Creator (utilitaire d'impression et de fusion de PDFs) http://www.pdfforge.org/pdfcreator

## Multimédia
- Photofiltre 7 (paint mais en mieux) http://www.photofiltre-studio.com/pf7-en.htm
- VLC (lecteur multimédia) http://www.videolan.org/vlc/

## Internet, Messagerie et Réseau
- Mozilla Firefox (Adblocks Plus et Ghostery !) https://www.mozilla.org/fr/firefox/desktop/
- Mozilla Thunderbird (Supporte jusqu'à 35 comptes de messagerie au moins !)
- Filezilla (THE client ftp) https://filezilla-project.org/
- Google Chrome (Navigateur alternatif) https://www.google.fr/chrome/
- Putty (THE client windows ssh) http://www.putty.org/
- OpenVPN (Client de connexion VPN) https://openvpn.net/index.php/open-source/downloads.html . Voir aussi la page d'aide dédiée à OpenVPN

## Programmation
- Code::Blocks (C et C++) http://www.codeblocks.org/downloads
- Notepad++ (éditeur de texte, couteau suisse, ouvre boite, décapssss) https://notepad-plus-plus.org/
- Smartgit, gratuit pour les étudiants, permet de gérer vos dépots git de façon simple http://www.syntevo.com/smartgit/
- GitKraken, comme smartgit, mais 100% free cette fois https://www.gitkraken.com/

## VoIP
- Mumble (opensource, léger) https://sourceforge.net/projects/mumble/
- Teamspeak (complet, simple à utiliser) https://www.teamspeak.com/downloads
- Skype (Microsoft) https://www.skype.com/fr/

## Utilitaires systèmes Antivirus
- Avast Antivirus Free (gratuit) https://www.avast.com/
- Mettre à jour son système ! Windows Update

## Divers mais très utile
- KeyPass (gestionnaire de mot de passe, open) http://keepass.info/
- 7zip (compression et décompression de fichiers) http://www.7-zip.org/ 


{{< /tab >}}
{{< tab "Linux" >}}
## Suites de bureautique
- Libreoffice (open-source <3) https://fr.libreoffice.org/

## Compléments en bureautique
- Foxit Reader (gratuit et léger !) https://www.foxitsoftware.com/fr/products/pdf-reader/

## Multimédia
- GIMP (Paint mais genre en vachement mieux) http://www.photofiltre-studio.com/pf7-en.htm
- VLC (lecteur multimédia) http://www.videolan.org/vlc/

## Internet, Messagerie et Réseau
- Mozilla Firefox (Adblocks Plus et Ghostery !) https://www.mozilla.org/fr/firefox/desktop/
- Mozilla Thunderbird (Supporte jusqu'à 35 comptes de messagerie au moins !)
- Filezilla (THE client ftp) https://filezilla-project.org/

## Programmation
- Sublim Text (éditeur de texte, couteau suisse, ouvre boite, décapssss) https://www.sublimetext.com/
- Eclipse (C++, Java) https://eclipse.org/downloads/

## VoIP
- Mumble (opensource, léger) https://sourceforge.net/projects/mumble/

## Utilitaires systèmes Antivirus
- Mettre à jour son système ! sudo apt-get update sudo apt-get upgrade !

{{< /tab >}}
{{< tab "OSx" >}}

## Suites de bureautique
- Microsoft Office

## Multimédia
- GIMP (Paint mais genre en vachement mieux) http://www.photofiltre-studio.com/pf7-en.htm
- VLC (lecteur multimédia) http://www.videolan.org/vlc/

## Internet, Messagerie et Réseau
- Safari
- Mail

## Programmation
- Xcode

## VoIP
- Mumble (opensource, léger) https://sourceforge.net/projects/mumble/
- Skype (Un peu de Windows dans votre Mac) https://www.skype.com/fr/

## Utilitaires systèmes Antivirus
- Mettre à jour son système !

{{< /tab >}}
{{< /tabs >}}
