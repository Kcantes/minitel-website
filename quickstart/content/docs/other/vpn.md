# Utilisation du VPN
Si vous avez besoin d'utiliser les serveurs de licence (kinshasa.emse.fr) ou les lecteurs réseaux (gcp-ens.emse.fr pour télécharger les logiciels enseignements) et que vous n'êtes pas à l'école, vous pouvez vous connecter au vpn pour pouvoir tout de même accéder à ces services.

Si vous avez déjà utilisé openvpn, ce sera un jeu d'enfant pour vous

## Récupération de la config vpn
- Se connecter sur le portail captif (oui, oui on peut le faire depuis un autre réseau que celui de l'école) https://fw-cgcp.emse.fr/auth
- Une fois connecté tu devrais avoir cet écran qui s'affiche
{{< imgs src="/img/Portail.png" >}}

- Cliquer sur le 3e lien (Profil VPN SSL pour clients OpenVPN)

Une fois cela fait, comme pour le vpn minitel, coller les fichiers de config dans le dossier vpn, et se connecter à openvpn_client et enfin entrer ses identifiants emse.

## Utilisation du client openvpn

{{< tabs "vpnecole" >}}

{{< tab "Windows" >}}
- Télécharger l'installateur ici: https://openvpn.net/index.php/open-source/downloads.html
- Dé-zipper le dossier openvpn_client.zip
- Copier/Coller ce dossier dans C:\Program Files\openvpn\config
- Lancer le logiciel openvpn GUI (se lance au démarrage depuis la version 2.4)
- Dans la barre tâches, sélectionner openvpn_client, puis rentrer ses identifiants école.

{{< imgs src="/img/Barre-tache.png" >}}
{{< imgs src="/img/Openvpn-gui.png" >}}



{{< /tab >}}

{{< tab "Linux" >}}
- dé-zipper le dossier openvpn_client.zip dans un dossier
- installer le paquet network-manager-openvpn-gnome 
        
        sudo apt-get install network-manager-openvpn-gnome

{{< imgs src="/img/Linux1.png" >}}
- Naviguer vers le dossier openvpn_client dé-zippé et selectionner le fichier openvpn_client.ovpn
- mettre ses identifiants école
- **IMPORTANT** : Dans cette fenetre de configuration dans les "Paramètres IPV4" allez, dans Routes ( en bas ) et cochez "Utiliser cette connexion uniquement pour les ressources de son réseau" afin de bénéficier du split tunneling
- Vous devriez voir une nouvelle connexion (qui devrait s'appeller openvpn_client)
{{< imgs src="/img/Linux2.png" >}}

- Si tout fonctionne vous devriez voir le message suivant:
{{< imgs src="/img/Linux3.png" >}}

{{< /tab >}}


{{< /tabs >}}

## Debug
Si cela semble ne pas fonctionner, vérifier que les résolutions DNS de quebec.emse.fr et kinshasa.emse.fr fonctionnent. Pour cela, ouvrir une console et écrire 

    ping quebec.emse.fr. 
Si l'ip s'affiche, c'est que tu es bien connecté au vpn. S'il ne répond pas, c'est normal car le ping est bloqué par le pare-feux.

Tu peux également écrire la commande netstat -nr et vérifier que les routes de l'école ont bien été ajoutées.

Tu peux aussi vérifier ton ip qui doit être de la forme 10.60.77.XX

Enfin, tu peux contacter minitel

- Sur messenger
- Sur g*
- par mail à minitel@emse.fr