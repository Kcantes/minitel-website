# Création Mailing list

Les listes de diffusion vous permettent de créer une adresse qui va rediriger vers plusieurs adresse.

Par exemple si vous voulez créer la liste pi_01@etu.emse.fr c'est possible, et quand cette boite recevra un mail, elle renvera ce mail à toute personne dans la liste.

**Attention toutefois** : ce système a deux limitations importantes: * cela ne fonctionne qu'entre les mails de l'école. Ex je veux envoyer un mail à PI01@etu.emse.fr depuis mon adresse gmail, ça ne marchera pas * en aucun cas il ne s'agit d'une adresse d'envoi, il ne sera donc pas possible d'envoyer un mail de la part de truk@etu.emse.fr

## Prerequis :
être connecté au réseau de l'école, ou au vpn de l'école

## utilisation de sympa
- Pour créer une liste de diffusion, il faut se connecter au site https://messel.emse.fr/sympa
- Il faut se connecter avec son adresse mail etu & le mot de passe école en haut à droite
- Pour créer une liste, il suffit de cliquer sur l'onglet "Création de liste"
- Je vous conseille ensuite de choisir "Paramétrage pour un groupe de travail"
- Remplir les champs "Objet", "Catégories" et "Description" et il ne vous reste plus qu'à attendre que les administrateurs (minitel en fait partit) valide votre liste
- En cliquant sur votre liste nouvellement créée, vous pouvez administrer les personnes qui y sont abonnées.

{{< imgs src="/img/Abonnements.png" >}}
