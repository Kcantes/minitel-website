# Statuts

Sur cette page sont inscrits les statuts de l'Association MINITEL. 

## Dénomination

Il est fondé entre les adhérents aux présents statuts et tous ceux qui s'y adjoindront, une association régie par la loi du 1er juillet 1901 et le décret du 16 Août 1901, ayant pour dénomination MINITEL.

## But
Cette association a pour but :

- Administrer le réseau de la résidence du site Georges Charpak Provence
- Offrir une gamme de services informatiques pour le campus

## Siège
Le siège social est fixé à :

    Hébergement Georges Charpak Provence
    879 Avenue de Mimet
    13120 Gardanne
## Durée de l'association
L’association est créée pour une durée illimitée. La dissolution pourra être effectuée conformément à l’article 16 des présents statuts. 
## Composition
L'association se compose de :

-    Adhérents
-    Administrateurs
-    Membres d'honneur
-    Membres bienfaiteurs

## Membres d'honneur et bienfaiteurs
Toute personne ayant apporté son aide à l'association peut prétendre au titre de membre d’honneur, cela inclut les anciens administrateurs. Ce titre est obtenu sous condition d'acceptation de la part du Conseil d'Administration lors d'un vote. Un membre d'honneur peut assister aux conseils d'administration et y tenir un rôle de consultant. Il a également un droit de vote lors des assemblées générales. Toute personne faisant don à l’association à titre définitif et irrévocable de biens et/ou services d’une valeur minimale de 200€, obtient, sous condition de l’acceptation du Conseil d’Administration par votation, le titre de membre bienfaiteur. 
## Admission
Pour adhérer à l’association, il suffit de remplir les conditions établies par le règlement intérieur. 
## Radiation et demission des membres actifs
La qualité de membre se perd par :

-    Démission
-    Décès
-    Exclusion prononcée à la majorité absolue, par la totalité du conseil d'administration, en cas de faute grave ou de manquement à ses responsabilités, d'un membre de l'association.
-    La perte de la qualité d'étudiant, de résident, ou de salarié du campus Georges Charpak Provence

Un membre dont la radiation serait proposée devra être convoqué par le Conseil d'Administration afin de permettre un dialogue entre le Conseil d'Administration et le membre concerné.

La radiation est acquise à la majorité des deux tiers des membres du Conseil d'Administration. 
## Ressources
Les ressources de l’association sont :

-    les cotisations fixées par le règlement intérieur.
-    les donations financières ou matérielles de sponsors ou membres de l’association.
-    les subventions.
-    les possibles recettes inhérentes aux activités de l’association.
-    toutes autres ressources autorisées par les textes législatifs réglementaires.

## Conseil d'administration
L’association est administrée entre deux Assemblées Générales Ordinaires par un Conseil d’Administration composé des membres du bureau et d'adhérents nommés par le bureau de l'association. En cas de vacances, le conseil pourvoit provisoirement au remplacement de ses membres. Le bureau pourra nommer un remplaçant définitif. Les pouvoirs des membres ainsi désignés prennent fin à l’époque où devrait normalement expirer le mandat des membres remplacés. 
## Membres du bureau
Le bureau est composé d’adhérents de l’association élus au scrutin secret par les membres de l’association. Il est composé de :

-    un président,
-    un secrétaire,
-    un trésorier.

Le bureau est élu une fois par an. 
## Rôle des membres du bureau
Le bureau se réunit à la demande d'au moins un de ses membres. Il veille au fonctionnement de l'association en conformité avec les orientations générales définies par l'Assemblée Générale et en application des décisions du Conseil d'Administration. Le bureau convoque les assemblées générales et les réunions du conseil d'administration. 
### Président
Il représente l'association dans tous les actes de la vie civile. Il peut déléguer certaines de ses attributions dans les conditions prévues au règlement intérieur. 
### Secrétaire
Le secrétaire est chargé de tout ce qui concerne la correspondance et les archives. Il rédige les procès-verbaux des délibérations et en assure la transcription sur les registres. 
### Trésorier
Le trésorier est chargé de tout ce qui concerne la gestion du patrimoine de l'association. Il effectue tous paiements et perçoit toutes recettes sous la surveillance du président. Il tient une comptabilité régulière au jour le jour de toutes les opérations et rend compte à l'assemblée annuelle, qui statue sur sa gestion. 

Les achats et ventes de valeurs mobilières constituant le fonds de réserve sont effectués avec l'autorisation du Conseil d'Administration. Toutefois, les dépenses supérieures au plafond fixé par le règlement intérieur et accepté par le conseil d'administration, doivent être ordonnancées par le président.

Il rend compte de son mandat aux assemblées générales dans les conditions prévues au règlement intérieur.

Toute opération bancaire d’un montant de plus de 150€ TTC doit être approuvée par un vote du Conseil d’Administration. 
## Assemblée générale ordinaire
Elle comprend tous les membres de l'association. Elle se réunit une fois par an, à l'initiative du président, sur convocation régulière par le secrétaire, une semaine au moins avant la date fixée. L'ordre du jour, fixé par le président, est indiqué sur les convocations.

Le président, assisté des membres du conseil d'administration, préside cette assemblée et expose la situation morale de l'association. Le trésorier rend compte de la gestion et soumet le bilan à l'approbation de l'assemblée. Il est procédé, après l'épuisement de l'ordre du jour, à l'élection du bureau pour le mandat suivant ainsi qu'à la validation ou non des bilans financiers et moraux présentés lors de cette assemblée. Ces votes doivent être réalisés lors d'un scrutin secret.

Devront être traitées, lors de cette assemblée, au moins les questions soumises à l'ordre du jour. Les délibérations sont adoptées à la majorité des membres présents ou représentés, nul ne peut être porteur de plus de trois mandats. La présence d'au moins 30 % des membres est exigée. Si ce quorum n'est pas atteint, l'assemblée est convoquée à nouveau à quinze jours au moins d'intervalle. Avec un ordre du jour strictement identique, elle peut alors délibérer valablement, quel que soit le nombre des présents. 
## Assemblée générale extraordinaire
Si besoin est, ou sur la demande de plus de la moitié des membres inscrits, le président peut convoquer une assemblée générale extraordinaire. Le quorum est fixé à 30%. Les convocations seront adressées aux membres par courrier électronique.

Elle se déroule comme une assemblée générale ordinaire avec pour ordre du jour exceptionnel la raison de sa convocation. 
## Règlement intérieur
Destiné à fixer les divers points non prévus par les présents statuts, notamment ceux qui ont trait à l'administration interne de l'association, le règlement intérieur sera établi et voté par le conseil d’administration. 
## Dissolution
La dissolution de l'association ne peut être prononcée que par l'assemblée générale extraordinaire convoquée spécialement à cet effet et statuant aux conditions de quorum et de majorité prévues pour les assemblées extraordinaires.

L'assemblée générale extraordinaire désigne un ou plusieurs commissaire(s) chargé(s) de la liquidation des biens de l'association dont elle déterminera les pouvoirs. Elle attribue l’actif net à toutes les associations de l'Ecole Nationale des Mines de Saint-Etienne campus Georges Charpak Provence, à part égale. 
