# Règlement Intérieur

Sur cette page est inscrit le règlement intérieur de l'Association MINITEL. 

## Dispositions générales

Le présent règlement ne peut être modifié que sur proposition et approbation par majorité simple du Conseil d’Administration. Ce règlement régit les différents points non précisés par les Statuts. En aucun cas, il ne peut fonder de disposition contraire aux Statuts. 
## Composition de l'association
L’association comprend 4 catégories de membres :

-    Les membres actifs dont la qualité s’acquiert dans les conditions exposées à l’Article 3 du Règlement Intérieur.
-    Les membres du Conseil d’Administration, ou Administrateurs, sont des membres actifs en charge de la gestion de l'association. Ils sont élèves au sein du cycle ISMIN de l’Ecole Nationale Supérieure des Mines de Saint-Etienne, dont le campus est situé sur le site Georges Charpak Provence (département des Bouches-du-Rhône). La qualité d’administrateur s’acquiert dans les circonstances fixées par l’article 10 des Statuts.
-    Les membres d'honneur et bienfaiteurs, dont les conditions d'adhésion sont définies dans l'article 6 des Statuts, sont exemptés de cotisation.

## Adhésion des membres actifs
Le candidat à l’adhésion doit fournir à l’association les documents suivants :

-    La cotisation, payée par chèque à l’ordre de MINITEL ou tout autre moyen de paiement accepté par le trésorier en fonction.
-    La fiche d’adhésion dûment remplie, signée à renouveler tous les ans.

Le montant de la cotisation annuelle est fixé à 5€. La signature du trésorier de l’association fera foi du paiement de la cotisation.

Le candidat obtient la qualité de membre actif à la date indiquée sur la fiche d’adhésion. 
## Effets de l'adhésion
L’adhésion à l’association est valable pendant un an à compter de la date inscrite sur la fiche d'adhésion et sous réserve du paiement de la cotisation.

L’adhésion entraîne la connaissance et l’acceptation des clauses prévues aux Statuts et au présent Règlement Intérieur. 
## Désignation du conseil d'administration et du bureau
Conformément à l’Article 10 des Statuts, le Conseil d’Administration est désigné par le Bureau. Le Bureau est élu lors d'une Assemblée Générale Ordinaire ayant lieu chaque année. Ces élections doivent être précédées par une convocation officielle des membres actifs pour la date fixée. L’ordre du jour de cette Assemblée Générale Ordinaire contient obligatoirement les points suivants: 
-    Vote du quitus moral et financier.
-    Élection du nouveau Bureau.

Le Bureau veille à l’organisation matérielle et au bon déroulement des élections. Celles-ci ont lieu par scrutin secret à un tour.

Chaque membre actif peut se voir confier au plus trois procurations de la part des membres absents lors de l’élection. Ces procurations doivent être cosignées par les deux membres et ce, en présence d'un membre du Bureau qui devra également apposer sa signature.

Tout membre du Conseil d’Administration peut déposer une réclamation s'il estime que le déroulement des élections n'a pas été conforme aux Statuts ou au présent Règlement Intérieur. Cette réclamation signée doit être présentée au Président sortant moins de deux jours ouvrables après l’élection. Le Bureau sortant est seul juge de sa recevabilité et peut décider de l’annulation de l’élection. Dans le cas où au moins un membre du Bureau entrant est également membre du Bureau sortant, la démarche s'effectue de manière similaire mais seuls les membres d'honneur de l'Association peuvent juger de la recevabilité de l'élection. 
## Prérogatives du bureau et du conseil d'administration
Les membres du Conseil d'Administration et du Bureau sont tenus de remplir leur mandat pendant une année entière. Sauf indication contraire, les décisions du Conseil d'Administration et du Bureau se prennent à la majorité simple; en cas d'égalité, la voix du Président est prépondérante.

Si une exclusion a lieu, le Conseil d’Administration en place désigne alors un nouveau responsable de poste, qui doit posséder la qualité de membre actif de l’association.

Une Assemblée Générale Extraordinaire doit alors être convoquée par le Secrétaire ou le Président en place pour désigner à la majorité simple un nouveau responsable de poste.

Un quorum de 50% est requis pour les votes ayant pour objet une démission ; la voix du président étant prépondérante en cas d’égalité. ## Admission
Pour adhérer à l’association, il suffit de remplir les conditions établies par le règlement intérieur. 
## Organisation interne
Les membres du Conseil d’Administration sont répartis en postes. Ils sont en charge de la gestion d’une partie de l’activité de l’association. 
## Responsabilités financières et contractuelles
Le Trésorier est responsable de la gestion administrative générale et de la trésorerie, conformément à l’Article 12 des Statuts. 

## Gestion des indemnités
Les membres de l’association ne peuvent recevoir aucune rétribution à raison des fonctions qui leur sont conférées.

Ils pourront toutefois obtenir le remboursement des dépenses engagées pour les besoins de l’association, sur justificatifs et après accord du Président. 
## Locaux
Tout usager se doit de se conformer au règlement intérieur de l'ENSM-SE vu la convention de mise à disposition du local, signée par les deux parties.

Tout membre ne possédant pas la qualité d’Administrateur doit y être accompagné d’au moins un membre du Conseil d’Administration. Par ailleurs, seuls les membres du bureau ont le droit de posséder une clé des locaux. Il est néanmoins possible de prêter une clé à un membre du Conseil d’Administration pour une durée déterminée. 
## Organisation de la passation
Pour assurer une passation de qualité entre les deux équipes successives, le Conseil d'Administration transmettra chaque année un Book de Passation devant comprendre les éléments suivants :

-    actions menées par chaque membre du Conseil d'Administration
-    idées d'actions à entreprendre
-    documentation relative aux services en place

Ce Book de Passation doit permettre de comprendre les tenants et aboutissants des services mis en place.

La passation officielle se déroule lors du premier Conseil d'Administration suivant l'Assemblée Générale Ordinaire. Les membres sortants sont tenus de former les membres entrants quant à leurs responsabilités et leurs missions au sein de l'association ainsi que sur leurs projets et engagements en cours. 
