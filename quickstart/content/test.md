---
title: Introduction
type: docs
---

# Bienvenue sur le Wiki Minitel
Bienvenue sur le wiki des utilisateurs du réseau de l'école des mines de saint-etienne campus provence. Celui-ci a été créé par Minitel pour aider les élèves dans l'utilisation des services de l'école.

**Voici la liste des pages disponibles pour vous !**

## L'association
- [Statuts de MINITEL](/docs/lassociation/statuts)
- [Règlement Intérieur de MINITEL](/docs/reglement)

## Pour l'école

- [Liste de logiciels recommandés ](/docs/Logiciels)
- [Paramètres pour utiliser un client mail tel que Thunderbird ou Outlook pour voir ses mails écoles ](/docs/mails)
- [Comment avoir l'agenda de l'école sur son téléphone?](/docs/agenda)

## Utilisation du matériel

- [Tuto installation d'une machine virtuelle](/docs/vm)
- [Tuto utilisation des imprimantes](/docs/imprimantes)
- [Tuto installation des lecteurs réseaux](/docs/lecteurs-reseaux)
- [Tuto d'utilisation du Seafile de l'école (cloud)](/docs/seafile)

## Autres

- [Tuto pour créer une mailing liste](/docs/mailing-list)
- [Tuto utilisation des imprimantes](/docs/imprimantes)
- [Utilisation du vpn de l'école pour les serveurs de licences et lecteurs réseaux](/docs/vpn-ecole)

## Loisirs

- [Tuto création d'un chroot sur une Debian Jessie](/docs/chroot)
- [Tuto création d'un VPN avec OpenVPN sur une Debian Jessie](/docs/openvpn)

