---
headless: true
bookMenuLevels: 1
---

 - [**Accueil**](/wiki/)
 - **L'association**
   - [Statuts](/docs/lassociation/statuts/)
   - [Règlement intérieur](/docs/lassociation/reglement/)
 - **Pour l'école**
   - [Logiciels](/docs/ecole/logiciels)
   - [Clients Mail](/docs/ecole/mail)
   - [Agenda](/docs/ecole/agenda)
 - **Utilisation du matériel**
   - [Machine virtuelle](/docs/materiel/vm)
   - [Imprimantes](/docs/materiel/imprimantes)
   - [Lecteurs réseaux](/docs/materiel/lecteurs-reseaux)
   - [Seafile](/docs/materiel/seafile)
   - [Eduroam](/docs/materiel/eduroam)
 - **Autres**
   - [Mailing list](/docs/other/mailing-list)
   - [VPN école](/docs/other/vpn)
 - **Loisirs**
  - [Chroot Debian](/docs/loisir/chroot)
  - [OpenVPN](/docs/loisir/openvpn)
