---
title: Introduction
type: docs
---

# Bienvenue sur le Wiki Minitel
Bienvenue sur le wiki des utilisateurs du réseau de l'école des mines de saint-etienne campus provence. Celui-ci a été créé par Minitel pour aider les élèves dans l'utilisation des services de l'école.

**Voici la liste des pages disponibles pour vous !**

## L'association
- [Statuts de MINITEL](/docs/lassociation/statuts)
- [Règlement Intérieur de MINITEL](/docs/lassociation/reglement)

## Pour l'école

- [Liste de logiciels recommandés ](/docs/ecole/logiciels)
- [Paramètres pour utiliser un client mail tel que Thunderbird ou Outlook pour voir ses mails écoles ](/docs/ecole/mail)
- [Comment avoir l'agenda de l'école sur son téléphone?](/docs/ecole/agenda)

## Utilisation du matériel

- [Tuto installation d'une machine virtuelle](/docs/materiel/vm)
- [Tuto utilisation des imprimantes](/docs/materiel/imprimantes)
- [Tuto installation des lecteurs réseaux](/docs/materiel/lecteurs-reseaux)
- [Tuto d'utilisation du Seafile de l'école (cloud)](/docs/materiel/seafile)
- [Utilisation du réseau wifi eduroam](/docs/materiel/eduroam)

## Autres

- [Tuto pour créer une mailing liste](/docs/other/mailing-list)
- [Utilisation du vpn de l'école pour les serveurs de licences et lecteurs réseaux](/docs/other/vpn)

## Loisirs

- [Tuto création d'un chroot sur une Debian Jessie](/docs/loisir/chroot)
- [Tuto création d'un VPN avec OpenVPN sur une Debian Jessie](/docs/loisir/openvpn)

